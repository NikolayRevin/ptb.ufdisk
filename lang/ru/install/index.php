<?
$MESS["ptb.ufdisk_MODULE_NAME"] = "Пользовательское свойство \"Привязка к диску\"";
$MESS["ptb.ufdisk_MODULE_DESC"] = "Пользовательское свойство для организации привязки к диску";
$MESS["ptb.ufdisk_PARTNER_NAME"] = "Ник-лаб";
$MESS["ptb.ufdisk_PARTNER_URI"] = "https://www.psdtobitrix.ru";
$MESS['ptb.ufdisk_UNINSTALL_TITLE'] = "Удаление модуля \"Оповещения в Slack\"";
$MESS['ptb.ufdisk_MODULE_INSTALL_ERROR'] = 'Модуль "Диск" не установлен!';
$MESS['ptb.ufdisk_INSTALL_TITLE'] = 'Установка модуля Пользовательское свойство "Привязка к диску"';
?>